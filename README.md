# Animate Image Sequence

A way to use a window scroll bar to scrub through an image sequence.

## Example

```Javascript
const videoHolderElements = document.querySelector('#video-holder');
const imgSeqElement = document.querySelector('.img-seq');

if (videoHolderElements && imgSeqElement) {
  new ImageVideoPlayer(
    205, // play speed
    5, // number of image to tween between
    1116, // total number of images
    videoHolderElements as HTMLElement, // render element outer holder
    imgSeqElement as HTMLImageElement, // render image element
    'img-seq/oceans' // base file name
  );
}
```

## Image sequence

The idea is that the images are sequenced like a stop action move or a old school flip book. So for example image should be number in order something like this `oceans0000` to `oceans1116` counting up.

## Demo

You can run the project locally with `yarn dev` or view [demo](https://codepen.io/Cagosto/pen/pEGrEV) and scroll down.
