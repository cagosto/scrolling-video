export default class AnimateImageSequence {
  private frameNumber: number = 0;
  private images: HTMLImageElement[] = [];
  private runVideo: number | undefined;
  private holdVal: number | undefined;

  constructor(
    private playSpeed: number,
    private imageRange: number,
    private totalImages: number,
    private videoHolder: HTMLElement,
    private imgSeq: HTMLImageElement,
    private imgFileName: string
  ) {
    this.initializeImages();
    this.videoHolder.style.height =
      (this.totalImages / this.imageRange) * this.playSpeed + 'px';
    this.scrollEvent = this.scrollEvent.bind(this);
    this.scrollPlay = this.scrollPlay.bind(this);
    window.addEventListener('scroll', this.scrollEvent);
  }

  private initializeImages() {
    for (let i = 0; i <= this.totalImages; i += this.imageRange) {
      let filename = this.imgFileName;
      let img = new Image();

      if (i < 10) {
        filename += '000';
      } else if (i < 100) {
        filename += '00';
      } else if (i < 1000) {
        filename += '0';
      }

      filename += i + '.jpg';
      img.src = filename;
      this.images.push(img);
    }
  }

  private scrollEvent(e: Event) {
    const pageY = window.scrollY;

    if (this.holdVal === pageY && this.runVideo) {
      window.cancelAnimationFrame(this.runVideo);
    } else {
      this.runVideo = window.requestAnimationFrame(this.scrollPlay);
    }
  }

  private scrollPlay() {
    this.frameNumber = Math.floor(window.scrollY / this.playSpeed);
    this.imgSeq.src = this.images[this.frameNumber].src;
  }
}

