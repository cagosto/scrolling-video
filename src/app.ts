import AnimateImageSequence from 'animate-image-sequence';

// Example usage
const videoHolderElements = document.querySelector('#video-holder');
const imgSeqElement = document.querySelector('.img-seq');

if (videoHolderElements && imgSeqElement) {
  new AnimateImageSequence(
    205,
    5,
    1116,
    videoHolderElements as HTMLElement,
    imgSeqElement as HTMLImageElement,
    'img-seq/oceans'
  );
}

