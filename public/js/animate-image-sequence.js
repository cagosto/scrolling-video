export default class AnimateImageSequence {
    constructor(playSpeed, imageRange, totalImages, videoHolder, imgSeq, imgFileName) {
        this.playSpeed = playSpeed;
        this.imageRange = imageRange;
        this.totalImages = totalImages;
        this.videoHolder = videoHolder;
        this.imgSeq = imgSeq;
        this.imgFileName = imgFileName;
        this.frameNumber = 0;
        this.images = [];
        this.initializeImages();
        this.videoHolder.style.height =
            (this.totalImages / this.imageRange) * this.playSpeed + 'px';
        this.scrollEvent = this.scrollEvent.bind(this);
        this.scrollPlay = this.scrollPlay.bind(this);
        window.addEventListener('scroll', this.scrollEvent);
    }
    initializeImages() {
        for (let i = 0; i <= this.totalImages; i += this.imageRange) {
            let filename = this.imgFileName;
            let img = new Image();
            if (i < 10) {
                filename += '000';
            }
            else if (i < 100) {
                filename += '00';
            }
            else if (i < 1000) {
                filename += '0';
            }
            filename += i + '.jpg';
            img.src = filename;
            this.images.push(img);
        }
    }
    scrollEvent(e) {
        const pageY = window.scrollY;
        if (this.holdVal === pageY && this.runVideo) {
            window.cancelAnimationFrame(this.runVideo);
        }
        else {
            this.runVideo = window.requestAnimationFrame(this.scrollPlay);
        }
    }
    scrollPlay() {
        this.frameNumber = Math.floor(window.scrollY / this.playSpeed);
        this.imgSeq.src = this.images[this.frameNumber].src;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWF0ZS1pbWFnZS1zZXF1ZW5jZS5qcyIsInNvdXJjZVJvb3QiOiIuLi9zcmMvIiwic291cmNlcyI6WyJhbmltYXRlLWltYWdlLXNlcXVlbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxPQUFPLE9BQU8sb0JBQW9CO0lBTXZDLFlBQ1UsU0FBaUIsRUFDakIsVUFBa0IsRUFDbEIsV0FBbUIsRUFDbkIsV0FBd0IsRUFDeEIsTUFBd0IsRUFDeEIsV0FBbUI7UUFMbkIsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUNqQixlQUFVLEdBQVYsVUFBVSxDQUFRO1FBQ2xCLGdCQUFXLEdBQVgsV0FBVyxDQUFRO1FBQ25CLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3hCLGdCQUFXLEdBQVgsV0FBVyxDQUFRO1FBWHJCLGdCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBQ3hCLFdBQU0sR0FBdUIsRUFBRSxDQUFDO1FBWXRDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDM0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUMvRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVPLGdCQUFnQjtRQUN0QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMzRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ2hDLElBQUksR0FBRyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7WUFFdEIsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNWLFFBQVEsSUFBSSxLQUFLLENBQUM7YUFDbkI7aUJBQU0sSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUNsQixRQUFRLElBQUksSUFBSSxDQUFDO2FBQ2xCO2lCQUFNLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRTtnQkFDbkIsUUFBUSxJQUFJLEdBQUcsQ0FBQzthQUNqQjtZQUVELFFBQVEsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDO1lBQ3ZCLEdBQUcsQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO1lBQ25CLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVPLFdBQVcsQ0FBQyxDQUFRO1FBQzFCLE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFFN0IsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUMvRDtJQUNILENBQUM7SUFFTyxVQUFVO1FBQ2hCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDdEQsQ0FBQztDQUNGIn0=