import AnimateImageSequence from 'animate-image-sequence';
// Example usage
const videoHolderElements = document.querySelector('#video-holder');
const imgSeqElement = document.querySelector('.img-seq');
if (videoHolderElements && imgSeqElement) {
    new AnimateImageSequence(205, 5, 1116, videoHolderElements, imgSeqElement, 'img-seq/oceans');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6Ii4uL3NyYy8iLCJzb3VyY2VzIjpbImFwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLG9CQUFvQixNQUFNLHdCQUF3QixDQUFDO0FBRTFELGdCQUFnQjtBQUNoQixNQUFNLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDcEUsTUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUV6RCxJQUFJLG1CQUFtQixJQUFJLGFBQWEsRUFBRTtJQUN4QyxJQUFJLG9CQUFvQixDQUN0QixHQUFHLEVBQ0gsQ0FBQyxFQUNELElBQUksRUFDSixtQkFBa0MsRUFDbEMsYUFBaUMsRUFDakMsZ0JBQWdCLENBQ2pCLENBQUM7Q0FDSCJ9