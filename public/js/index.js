export default class AnimateImageSequence {
    constructor(playSpeed, imageRange, totalImages, videoHolder, imgSeq, imgFileName) {
        this.playSpeed = playSpeed;
        this.imageRange = imageRange;
        this.totalImages = totalImages;
        this.videoHolder = videoHolder;
        this.imgSeq = imgSeq;
        this.imgFileName = imgFileName;
        this.frameNumber = 0;
        this.images = [];
        this.initializeImages();
        this.videoHolder.style.height =
            (this.totalImages / this.imageRange) * this.playSpeed + 'px';
        this.scrollEvent = this.scrollEvent.bind(this);
        this.scrollPlay = this.scrollPlay.bind(this);
        window.addEventListener('scroll', this.scrollEvent);
    }
    initializeImages() {
        for (let i = 0; i <= this.totalImages; i += this.imageRange) {
            let filename = this.imgFileName;
            let img = new Image();
            if (i < 10) {
                filename += '000';
            }
            else if (i < 100) {
                filename += '00';
            }
            else if (i < 1000) {
                filename += '0';
            }
            filename += i + '.jpg';
            img.src = filename;
            this.images.push(img);
        }
    }
    scrollEvent(e) {
        const pageY = window.scrollY;
        if (this.holdVal === pageY && this.runVideo) {
            window.cancelAnimationFrame(this.runVideo);
        }
        else {
            this.runVideo = window.requestAnimationFrame(this.scrollPlay);
        }
    }
    scrollPlay() {
        this.frameNumber = Math.floor(window.scrollY / this.playSpeed);
        this.imgSeq.src = this.images[this.frameNumber].src;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiLi4vc3JjLyIsInNvdXJjZXMiOlsiaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLE9BQU8sT0FBTyxvQkFBb0I7SUFNdkMsWUFDVSxTQUFpQixFQUNqQixVQUFrQixFQUNsQixXQUFtQixFQUNuQixXQUF3QixFQUN4QixNQUF3QixFQUN4QixXQUFtQjtRQUxuQixjQUFTLEdBQVQsU0FBUyxDQUFRO1FBQ2pCLGVBQVUsR0FBVixVQUFVLENBQVE7UUFDbEIsZ0JBQVcsR0FBWCxXQUFXLENBQVE7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQVE7UUFYckIsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFDeEIsV0FBTSxHQUF1QixFQUFFLENBQUM7UUFZdEMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUMzQixDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQy9ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRU8sZ0JBQWdCO1FBQ3RCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQzNELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDaEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztZQUV0QixJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ1YsUUFBUSxJQUFJLEtBQUssQ0FBQzthQUNuQjtpQkFBTSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUU7Z0JBQ2xCLFFBQVEsSUFBSSxJQUFJLENBQUM7YUFDbEI7aUJBQU0sSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFO2dCQUNuQixRQUFRLElBQUksR0FBRyxDQUFDO2FBQ2pCO1lBRUQsUUFBUSxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUM7WUFDdkIsR0FBRyxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUM7WUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBRU8sV0FBVyxDQUFDLENBQVE7UUFDMUIsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUU3QixJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDM0MsTUFBTSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQy9EO0lBQ0gsQ0FBQztJQUVPLFVBQVU7UUFDaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUN0RCxDQUFDO0NBQ0YifQ==