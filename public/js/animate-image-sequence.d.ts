export default class AnimateImageSequence {
    private playSpeed;
    private imageRange;
    private totalImages;
    private videoHolder;
    private imgSeq;
    private imgFileName;
    private frameNumber;
    private images;
    private runVideo;
    private holdVal;
    constructor(playSpeed: number, imageRange: number, totalImages: number, videoHolder: HTMLElement, imgSeq: HTMLImageElement, imgFileName: string);
    private initializeImages;
    private scrollEvent;
    private scrollPlay;
}
//# sourceMappingURL=animate-image-sequence.d.ts.map